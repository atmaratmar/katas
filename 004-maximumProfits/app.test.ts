const maxStockProfit = require('./app.js')
// Comparing Strings
test('The maximum profit', () => {
	var allElement=[8, 5, 12, 9, 19, 1]	
	expect(maxStockProfit(allElement)).toEqual(14);
});
test('the maximum profit give 0', () => {
	var allElement=[21, 12, 11, 9, 6, 3]	
	expect(maxStockProfit(allElement)).toEqual(0);
});
test('the maximum profit not return 7', () => {
	var allElement=[2, 4, 9, 3, 8]	
	expect(maxStockProfit(allElement)).not.toEqual(0);
});