const maxStockProfit = arr => {

    let maxProfit = 0; // initialize max

    let lowestPrice = arr[0];
   
    for(let i = 1; i < arr.length; i++){

        let price = arr[i];

        if(price < lowestPrice) lowestPrice = price;

        let profit = price - lowestPrice;

        maxProfit = Math.max(profit, maxProfit);

    }
    return maxProfit;

}
module.exports = maxStockProfit;
// const a =maxStockProfit([8, 5, 12, 9, 19, 1]);
// const b = maxStockProfit([21, 12, 11, 9, 6, 3]);
// console.log(a)
// console.log(b)

